import { BrowserModule,HAMMER_LOADER,HammerGestureConfig} from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from  '@angular/router';
import { AppComponent } from './app.component';
import { TodoAppComponent } from './components/todo-app/todo-app.component';
import { TodoService } from './services/todo.service';
import { CompletedDirective } from './directives/completed.directive';
import { CustomUppercasePipe } from './pipes/custom-uppercase.pipe';
import { SearchPipe } from './pipes/search.pipe';
import { SortPipe } from './pipes/sort.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { MatNativeDateModule } from "@angular/material";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { MatDialogModule } from "@angular/material";
import { MatToolbarModule, 
         MatIconModule,
        MatSidenavModule,
        MatListModule,
        MatButtonModule,
        MatCheckboxModule} from  '@angular/material';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import { LocationStrategy, HashLocationStrategy } from "@angular/common";
// import {
//   MatSidenav
// } from '@angular/material';
const routes: Routes = [
  { path: '', redirectTo: '/todo', pathMatch: 'full'},
  // { path: 'login', component: LoginFormComponent },
  // { path: 'register', component: RegFormComponent },
  // { path: 'todo/:id', component: TodoDetailComponent, canActivate: [AuthGuard] },
  // { path: 'todo', component: TodoAppComponent, canActivate: [AuthGuard] },
  { path: 'todo', component: TodoAppComponent, },
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    // CounterComponent,
    // SubscriptionFormComponent,
    // LoginFormComponent,
    TodoAppComponent,
     CompletedDirective,
    // DelayDirective,
    // HighlightDirective,
    CustomUppercasePipe,
    SearchPipe,
    SortPipe,
    // MatSidenav
    // RegFormComponent,
    // ReactiveRegFormComponent,
    // TodoDetailComponent,
    // PageNotFoundComponent    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    // MatSidenav,
    ReactiveFormsModule,
    NoopAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatNativeDateModule,
    BrowserAnimationsModule,
    MatDialogModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatInputModule
  ],
  providers: [ TodoService,
    { provide: LocationStrategy, useClass: HashLocationStrategy }, ],
  
  bootstrap: [AppComponent]
})
export class AppModule { }
