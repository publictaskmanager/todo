import { Component, OnInit,ChangeDetectorRef  } from '@angular/core';
import { BrowserModule } from "@angular/platform-browser";
import { CommonModule } from "@angular/common";
import { TodoService } from '../../services/todo.service';
import { Task } from '../../models/task';
import { NgModule } from "@angular/core";
import { MediaMatcher } from '@angular/cdk/layout';

@Component({
  selector: 'app-todo-app',
  templateUrl: './todo-app.component.html',
  styleUrls: ['./todo-app.component.css']
})
@NgModule({
  imports: [CommonModule, BrowserModule],
  declarations: []
})
export class TodoAppComponent implements OnInit {

  authorized: boolean = false;

  tasks: Task[] = [];
  task: String = '';
  errorMsg: String = '';
  successMsg: String = '';
  editIndex: number = -1;
  query: String = '';
  mobileQuery: MediaQueryList;
  fillerNav = Array.from({ length: 5 }, (_, i) => `Nav Item ${i + 1}`);
  private _mobileQueryListener: () => void;
  constructor(
    private todo: TodoService, 
    changeDetectorRef: ChangeDetectorRef,
      media: MediaMatcher,   
  ) {

    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

   }

  ngOnInit() {

    // if(localStorage.getItem('user')) {
    //   this.authorized = true;
    // }

    this.tasks = this.todo.getAllTasks(); //Calling the getAllTasks() method from Service
  }

  completeTask(id) {
    this.todo.completeTask(id);
    this.tasks = this.todo.getAllTasks(); //Calling the getAllTasks() method from Service
  }

  resetTask(id) {
    this.todo.resetTask(id);
    this.tasks = this.todo.getAllTasks();
  }

  addTask(e) {
    e.preventDefault();

    if( this.task === '') {
      //error
      this.errorMsg = 'Task can not be blank.';
    } else {
      this.todo.addTask(this.task); //Calling the Add method from Service
      this.successMsg = 'Task Added Successfully!';

      this.task = '';
      setTimeout(() => this.successMsg = '', 3000);      
    }
  }

  handleChange(e) {
    if(e.keyCode !== 13) {
      this.errorMsg = '';
      this.successMsg = '';
    }
  }

  // editTask(index) {
  //   let tempTask = this.todo.getSingleTask(index);
  //   this.task = tempTask.title;
  //   this.editIndex = index;
  // }

  saveTask(e) {
    e.preventDefault();
    this.todo.editTask(this.task, this.editIndex);

    this.successMsg = 'Task Edited Successfully!';
    setTimeout(() => this.successMsg = '', 3000);

    this.tasks = this.todo.getAllTasks();

    this.editIndex = -1;
    this.task = '';
  }

  removeTask(index) {
    let confirmed = confirm('Are you sure?');

    if(confirmed === true) {
      let removedTask = this.todo.removeTask(index); //Calling the Remove method from Service
      this.successMsg = removedTask[0].title + ' Removed Successfully!';
  
      setTimeout(() => this.successMsg = '', 3000);      
    }
  }

}
