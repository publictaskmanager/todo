import { Directive, ElementRef, Input, OnInit, OnChanges } from '@angular/core';

@Directive({
  selector: '[appCompleted]'
})

export class CompletedDirective implements OnInit, OnChanges {

  @Input() completed: boolean;

  taskComplete() {
    if(this.completed === true) {
      this.el.nativeElement.style.textDecoration = 'line-through';
    }else{
        this.el.nativeElement.style.textDecoration='none';
    }
  }

  ngOnInit() {
    this.taskComplete();
  }

  ngOnChanges() {
    this.taskComplete();
  }

  constructor(private el: ElementRef) {
  
  }
  
}