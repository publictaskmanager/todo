import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(value: any, args?: any): any {    
    let filteredItems  = value.filter(el => {
      if(el.title.toUpperCase().search(args.toUpperCase()) > -1) {
        return true;
      } else {
        return false;
      }
    });
    
    return filteredItems;
  }

}
