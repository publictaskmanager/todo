import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value.sort(this.compare);
  }

  compare(a, b) {
    const titleA = a.title;
    const titleB = b.title;

    if(titleA > titleB) {
      return 1;
    } else {
      return -1;
    }
  }
}
