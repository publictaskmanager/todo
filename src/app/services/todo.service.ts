import { Injectable } from '@angular/core';
import { Task } from '../models/task';

@Injectable()
export class TodoService {

  tasks: Task[] = [
    {
      _id: 1,
      title: 'Learn MongoDB',
      completed: false,
      date: new Date()
    },
    {
      _id: 2,
      title: 'Learn Angular',
      completed: false,
      date: new Date()
    },
    {
      _id: 3,
      title: 'Learn NodeJS',
      completed: false,
      date: new Date()
    }
  ];

  constructor() { }

  completeTask(id) {
    this.tasks.forEach( (task: Task) => {
      if(task._id === id) {
        task.completed = true;
      }
    })
  }

  resetTask(id: number) {
    this.tasks.forEach( (task: Task) => {
      if(task._id === id) {
        task.completed = false;
      }
    } );
  }

  getAllTasks() { 
    return this.tasks;
  }

  addTask(task: String) {
    let newTask: Task = {
      _id: this.tasks.length,
      title: task,
      completed: false,
      date: new Date()
    }
    this.tasks.push(newTask);
  }

  removeTask(index: number) {
    return this.tasks.splice(index, 1);
  }

  getSingleTask(id: Number): Task {
    let temptask:Task = null;
    this.tasks.forEach(task => {
      if(task._id === id) {
        temptask = task;
      }
    });

    return temptask;
  }

  editTask(task: String, index: number) {
    let newTask: Task = {
      _id: this.tasks.length,
      title: task,
      completed: false,
      date: new Date()
    }
    this.tasks[index] = newTask;
  }

}
